import 'package:flutter_modular/flutter_modular.dart';
import 'package:project_wmb/app/modules/pokemon/pokemon_list/pokemon_list_module.dart';
import 'package:project_wmb/app/modules/pokemon/pokemon_list/pokemon_list_page.dart';
import 'package:project_wmb/app/modules/pokemon/pokemon_new/pokemon_new_module.dart';
import 'package:project_wmb/app/modules/pokemon/pokemon_new/pokemon_new_page.dart';

class AppModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => PokemonListPage()),
    Bind.lazySingleton((i) => PokemonNewPage()),
  ];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute(Modular.initialRoute, module: PokemonListModule()),
    ModuleRoute('/add', module: PokemonNewModule()),
  ];
}
