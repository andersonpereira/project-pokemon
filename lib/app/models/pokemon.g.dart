// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pokemon _$PokemonFromJson(Map<String, dynamic> json) {
  return Pokemon(
    json['uid'] as String,
    json['name'] as String,
    json['url'] as String,
    json['height'] as int,
    json['weight'] as int,
    json['type'] as String,
  );
}

Map<String, dynamic> _$PokemonToJson(Pokemon instance) => <String, dynamic>{
      'uid': instance.uid,
      'name': instance.name,
      'url': instance.url,
      'height': instance.height,
      'weight': instance.weight,
      'type': instance.type,
    };
