import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';

part 'pokemon.g.dart';

@JsonSerializable()
class Pokemon {
  final String uid;
  final String name;
  final String url;
  final int height;
  final int weight;
  final String type;

  static List<String> types = ['Select a type ', 'Other'];

  Pokemon(this.uid, this.name, this.url, this.height, this.weight, this.type);

  factory Pokemon.fromJson(Map<String, dynamic> json) =>
      _$PokemonFromJson(json);

  Map<String, dynamic> toJson() => _$PokemonToJson(this);

  Pokemon.fromFirestoresnapshot(QueryDocumentSnapshot<Object?> snap)
      : uid = snap.id,
        name = snap.get('name'),
        height = 0,
        weight = 0,
        type = '',
        url = snap.get('url');
}
