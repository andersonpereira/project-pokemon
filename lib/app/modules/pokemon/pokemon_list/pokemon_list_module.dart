import 'package:project_wmb/app/modules/pokemon/pokemon_list/pokemon_list_page.dart';
import 'package:project_wmb/app/modules/pokemon/pokemon_list/pokemon_list_store.dart';
import 'package:flutter_modular/flutter_modular.dart';

class PokemonListModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => PokemonListStore()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('/', child: (_, args) => PokemonListPage()),
  ];
}
