import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:project_wmb/app/modules/pokemon/pokemon_list/pokemon_list_store.dart';
import 'package:flutter/material.dart';

class PokemonListPage extends StatefulWidget {
  final String title;
  const PokemonListPage({Key? key, this.title = 'Pokémons'}) : super(key: key);
  @override
  PokemonListPageState createState() => PokemonListPageState();
}

class PokemonListPageState extends State<PokemonListPage> {
  final PokemonListStore store = Modular.get();

  @override
  Widget build(BuildContext context) {
    store.getFromFirestore();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(children: <Widget>[
        Observer(
            builder: (_) => Flexible(
                child: ListView.builder(
                    itemCount: store.pokemons.value!.length,
                    itemBuilder: (context, index) {
                      return new InkWell(
                          onTap: () {
                            Modular.to.pushNamed('/add',
                                arguments: store.pokemons.value![index]);
                          },
                          child: Dismissible(
                            key: Key(store.pokemons.value![index].uid),
                            direction: DismissDirection.startToEnd,
                            background: Container(
                              color: Colors.redAccent.shade200,
                            ),
                            onDismissed: (direction) {
                              if (direction == DismissDirection.startToEnd) {
                                store.delFromFirestore(
                                    store.pokemons.value![index]);
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Card(
                                child: ListTile(
                                  leading: Icon(Icons.catching_pokemon,
                                      color: Colors.red),
                                  title: Text(
                                      '${store.pokemons.value![index].name}'),
                                  subtitle: Text(
                                      '${store.pokemons.value![index].url}'),
                                ),
                              ),
                            ),
                          ));
                    })))
      ]),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Modular.to.pushNamed('/add');
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
