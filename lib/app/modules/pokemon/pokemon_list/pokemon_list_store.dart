import 'package:mobx/mobx.dart';
import 'package:project_wmb/app/models/pokemon.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

part 'pokemon_list_store.g.dart';

class PokemonListStore = _PokemonListStoreBase with _$PokemonListStore;

abstract class _PokemonListStoreBase with Store {
  @observable
  late Pokemon pokemon;

  @observable
  ObservableFuture<List<Pokemon>> pokemons =
      ObservableFuture<List<Pokemon>>.value([]);

  @computed
  List<Pokemon>? get getItensFromFirestore => pokemons.value;

  @action
  Future<bool> getFromFirestore() async {
    Query objRef =
        FirebaseFirestore.instance.collection('pokemon').orderBy('name');

    QuerySnapshot collectionSnapshot = await objRef.get();

    List<Pokemon> pokemons = [];

    collectionSnapshot.docs.forEach((element) {
      pokemons.add(Pokemon.fromFirestoresnapshot(element));
    });
    this.pokemons = ObservableFuture<List<Pokemon>>.value(pokemons);

    return Future.value(true);
  }

  @action
  Future<bool> delFromFirestore(Pokemon pokemon) async {
    FirebaseFirestore.instance.collection('pokemon').doc(pokemon.uid).delete();

    return Future.value(true);
  }
}
