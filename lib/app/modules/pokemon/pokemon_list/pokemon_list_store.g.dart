// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_list_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PokemonListStore on _PokemonListStoreBase, Store {
  Computed<List<Pokemon>?>? _$getItensFromFirestoreComputed;

  @override
  List<Pokemon>? get getItensFromFirestore =>
      (_$getItensFromFirestoreComputed ??= Computed<List<Pokemon>?>(
              () => super.getItensFromFirestore,
              name: '_PokemonListStoreBase.getItensFromFirestore'))
          .value;

  final _$pokemonAtom = Atom(name: '_PokemonListStoreBase.pokemon');

  @override
  Pokemon get pokemon {
    _$pokemonAtom.reportRead();
    return super.pokemon;
  }

  @override
  set pokemon(Pokemon value) {
    _$pokemonAtom.reportWrite(value, super.pokemon, () {
      super.pokemon = value;
    });
  }

  final _$pokemonsAtom = Atom(name: '_PokemonListStoreBase.pokemons');

  @override
  ObservableFuture<List<Pokemon>> get pokemons {
    _$pokemonsAtom.reportRead();
    return super.pokemons;
  }

  @override
  set pokemons(ObservableFuture<List<Pokemon>> value) {
    _$pokemonsAtom.reportWrite(value, super.pokemons, () {
      super.pokemons = value;
    });
  }

  final _$getFromFirestoreAsyncAction =
      AsyncAction('_PokemonListStoreBase.getFromFirestore');

  @override
  Future<bool> getFromFirestore() {
    return _$getFromFirestoreAsyncAction.run(() => super.getFromFirestore());
  }

  @override
  String toString() {
    return '''
pokemon: ${pokemon},
pokemons: ${pokemons},
getItensFromFirestore: ${getItensFromFirestore}
    ''';
  }
}
