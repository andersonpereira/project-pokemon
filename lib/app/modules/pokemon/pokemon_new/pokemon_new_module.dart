import 'package:flutter_modular/flutter_modular.dart';
import 'package:project_wmb/app/modules/pokemon/pokemon_new/pokemon_new_page.dart';
import 'package:project_wmb/app/modules/pokemon/pokemon_new/pokemon_new_store.dart';

class PokemonNewModule extends Module {
  @override
  final List<Bind> binds = [
    Bind.lazySingleton((i) => PokemonNewStore()),
  ];

  @override
  final List<ModularRoute> routes = [
    ChildRoute('add', child: (_, args) => PokemonNewPage()),
  ];
}
