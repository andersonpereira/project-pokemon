import 'package:flutter_modular/flutter_modular.dart';
import 'package:project_wmb/app/models/pokemon.dart';
import 'package:project_wmb/app/modules/pokemon/pokemon_new/pokemon_new_store.dart';
import 'package:flutter/material.dart';

class PokemonNewPage extends StatefulWidget {
  final String title;
  const PokemonNewPage({Key? key, this.title = 'Adicionar Novo'})
      : super(key: key);
  @override
  PokemonNewPageState createState() => PokemonNewPageState();
}

class PokemonNewPageState extends State<PokemonNewPage> {
  final PokemonNewStore store = Modular.get();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _height = TextEditingController();
  final TextEditingController _weight = TextEditingController();

  String type = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
          padding: EdgeInsets.all(20.0),
          child: Form(
              child: ListView(children: <Widget>[
            TextFormField(
              autovalidateMode: AutovalidateMode.always,
              controller: _name,
              decoration: InputDecoration(
                  icon: Icon(Icons.description),
                  labelText: 'Name',
                  fillColor: Colors.white),
              autocorrect: false,
            ),
            TextFormField(
              autovalidateMode: AutovalidateMode.always,
              controller: _height,
              keyboardType: new TextInputType.numberWithOptions(decimal: true),
              decoration: InputDecoration(
                  icon: Icon(Icons.height),
                  labelText: 'Height',
                  fillColor: Colors.white),
              autocorrect: false,
            ),
            TextFormField(
              autovalidateMode: AutovalidateMode.always,
              controller: _weight,
              keyboardType: new TextInputType.numberWithOptions(decimal: true),
              decoration: InputDecoration(
                  icon: Icon(Icons.monitor_weight),
                  labelText: 'weight',
                  fillColor: Colors.white),
              autocorrect: false,
            ),
            /*  DropdownButtonFormField<String>(
              value: type,
              iconSize: 24,
              elevation: 16,
              decoration: InputDecoration(
                  icon: Icon(Icons.local_activity),
                  labelText: 'Type',
                  fillColor: Colors.white),
              onChanged: (String? newValue) {
                /* setState(() {
                  type = newValue!;
                }); */
              },
              items:
                  Pokemon.types.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                    value: value, child: Text(value));
              }).toList(),
            ), */
            Padding(
                padding: EdgeInsets.symmetric(vertical: 100),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    MaterialButton(
                      child: Text(
                        'Save',
                        style: TextStyle(color: Colors.white),
                      ),
                      padding: const EdgeInsets.all(10.0),
                      onPressed: () {
                        var pokemon = Pokemon(
                            '',
                            _name.text,
                            '',
                            int.tryParse(_height.text)!,
                            int.tryParse(_weight.text)!,
                            type);
                        store
                            .pushToFirestore(pokemon)
                            .then((result) => Navigator.pop(context));
                        _name.clear();
                        _height.clear();
                        _weight.clear();
                      },
                      color: Colors.blue,
                    ),
                  ],
                )),
          ]))),
    );
  }
}
