const fs = require("fs");
const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

// Initialize Cloud Firestore through Firebase
var firebaseConfig = {
    apiKey: "AIzaSyDLt78-e1erkdFR39_ri4OulydBLET2jd0",
    authDomain: "projeto-wmb.firebaseapp.com",
    projectId: "projeto-wmb",
    storageBucket: "projeto-wmb.appspot.com",
    messagingSenderId: "372855402155",
    appId: "1:372855402155:web:6cf900b899584348f511ee",
    measurementId: "G-XB2DJJ6QGY"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  
var db = firebase.firestore();

fs.readFile("./json_data_seed.json", "utf8", (err, jsonString) => {
    if (err) {
      console.log("File read failed:", err);
      return;
    }
    var data  = JSON.parse(jsonString);

    data.pokemon.forEach(function(obj) {
        db.collection("pokemon").add({
            name: obj.name,
            url: obj.url
        }).then(function(docRef) {
            console.log("Document written with ID: ", docRef.id);
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
        });
    });
  });

  /// RUN : node json_to_firestore_seed.js

